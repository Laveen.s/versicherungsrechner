import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './Home';
import Versicherungsrechner from './Versicherungsrechner';
import Quadratmetermodell from './Quadratmetermodell';
import NavigationBar from './NavigationBar';
import './App.css';

function App() {
  return (
    <Router>
      <NavigationBar />
      <Routes>
        <Route exact path="/Home" element={<Home/>} />
        <Route path="/Versicherungsrechner" element={<Versicherungsrechner/>} />
        <Route path="/Quadratmetermodell" element={<Quadratmetermodell/>} />
      </Routes>
    </Router>
  );
}

export default App;

