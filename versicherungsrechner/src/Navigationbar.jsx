import React from 'react';
import { Link } from 'react-router-dom';
import './NavigationBar.css';

function NavigationBar() {
  return (
    <nav>
      <ul className="navigation-menu">
        <li><Link to="/Home">Home</Link></li>
        <li><Link to="/Versicherungsrechner">Versicherungsrechner</Link></li>
        <li><Link to="/Quadratmetermodell">Quadratmetermodell</Link></li>
      </ul>
    </nav>
  );
}

export default NavigationBar;





