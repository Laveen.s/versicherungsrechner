import './Versicherungsrechner.css';
import { useState } from 'react';


export default function Quadratmetermodell() {

  

  const [inputSquareMeter, setQuadratMeter] = useState('')
  const [inputLumbSum, setLumbSum] = useState('')
  const [insuranceSum, setInsuranceSum] = useState('')

  

  

  function berechnen2() {
    let inputSquareMeterAsNumber = parseInt(inputSquareMeter);
    let inputLumbSumAsNumber = parseInt(inputLumbSum);


    
    setInsuranceSum(inputSquareMeterAsNumber*inputLumbSumAsNumber)

  }

  


  return (
    <div className="Quadratmetermodell" >
      

      <h1>Richtige Versicherungssumme kalkulieren</h1>
      <h2>Quadratmetermodell</h2>
      <p>Anzahl Quadratmeter in der Wohnung/Haus: <input type="text" value={inputSquareMeter} onInput={e => setQuadratMeter(e.target.value)}></input><br /></p>
      <p>Pauschalbetrag pro Quadratmeter: <input type="text" value={inputLumbSum} onInput={e => setLumbSum(e.target.value)}></input><br /></p>
      <p><input type="button" value="Berechnen" onClick={berechnen2}></input></p>
      <p>Ihre richtige Versichersumme wäre etwa {insuranceSum}.-</p>

    </div>
    


  );
}

