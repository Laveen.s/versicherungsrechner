import './Versicherungsrechner.css';
import React,{ useState } from 'react';



function Versicherungsrechner() {

    const [inputSum, setInputSumme] = useState('')
    const [inputValue, setinputValue] = useState('')
    const [inputDamage, setinputDamage] = useState('')
    const [outcome, setOutcome] = useState('')
    const [hedgingInPercent, setHIP] = useState('')

    function berechnen() {
        let inputSumAsNumber = parseInt(inputSum);
        let inputValueAsNumber = parseInt(inputValue);
        let inputDamageAsNumber = parseInt(inputDamage);
        
        
        if (inputSumAsNumber > inputValueAsNumber){
          setOutcome(inputValueAsNumber)
          
        } else {
          setOutcome(inputSumAsNumber/inputValueAsNumber*inputDamageAsNumber)
    
        }
        setHIP(100/inputValueAsNumber*inputSumAsNumber)
        
      }

    return (
        <div className="Versicherungsrechner" >

          <h1>Versicherungsrechner für Schaden</h1>
          <p>Versicherungssumme: <input type="text" value={inputSum} onInput={e => setInputSumme(e.target.value)}></input><br /></p>
          <p>Versicherungswert: <input type="text" value={inputValue} onInput={e => setinputValue(e.target.value)}></input><br /></p>
          <p>Schaden: <input type="text" value={inputDamage} onInput={e => setinputDamage(e.target.value)}></input><br /></p>
          <p><input type="button" value="Berechnen" onClick={berechnen}></input></p>
    
          <p>Ergebnis: Sie sind zu {hedgingInPercent}% versichert und die Versicherung zahlt Ihnen {outcome}.- vom Schaden</p>
    
        </div>
        
    
    
      );
    }
    
    export default Versicherungsrechner;