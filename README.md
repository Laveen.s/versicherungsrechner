#Einleitung


Diese Dokumentation ist über ein Projekt, das ich in diesem Fach Prog 1 durchgeführt habe. Ich habe den Auftrag erhalten einen Versicherungsrechner mit React zu programmieren. Es wurde unserer Klasse viel Raum gelassen, damit wir selbst bestimmen können, was dieser Versicherungsrechner genau tun soll. Ich habe mich dazu entschieden einen Schadensrechner beim Haushalt zu machen, wie wir es in der KBW gelernt bekommen haben. Der Schadensrechner berechnet, mithilfe der Versicherungssumme, dem aktuellen Versicherungswert und dem erlittenen Schaden, den Betrag, den die Versicherung bezahlen würde. Darüber hinaus habe ich noch ein Quadratmetermodell miteingebaut, das errechnen soll, wie gross der Wert des Haushaltes überhaupt ist. Das Problem ist, dass ich, nicht wie die meisten anderen aus meiner klasse, keine Erfahrung mit React habe.

Weitere Informationen -> Siehe Word-Dokumentation 'Versicherungsrechnerdoku.docx'
Versicherungsrechner-Projekt -> 'versicherungsrechner'
Planung zu Beginn (Userstory, Keyscreen, Problemstatement, Systemdiagramm) -> '12.2-Idee-entwickeln.pdf'
